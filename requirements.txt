Django==1.7.4
django-braces==1.4.0
django-filter==0.9.2
djangorestframework==3.1.1
Pillow==2.8.0
six==1.9.0
