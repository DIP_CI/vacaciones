from django.shortcuts import render
from django.views.generic import CreateView
from django.core.urlresolvers import reverse_lazy
from django.http import HttpResponse
from braces import views
import json
from .models import *
from .serializers import *
from rest_framework import viewsets, generics, filters
from django.http import Http404
from rest_framework.views import APIView
from rest_framework.decorators import api_view
from rest_framework.response import Response
from rest_framework import status
import codecs
from rest_framework import viewsets
from django.contrib.auth.decorators import login_required

# Create your views here.
# @login_required
class index_area(CreateView):
	template_name= 'index_Area_Cargo.html'
	model = Area
	context_object_name = 'Lista_Area'
	success_url = reverse_lazy('Area')
	
# @login_required
class cargo(CreateView):
	template_name= 'cargo.html'
	model = Cargo
	context_object_name = 'Lista_Cargo'
	success_url = reverse_lazy('Cargo')	

# @login_required
class AreasViewSet(viewsets.ModelViewSet):
	serializer_class = AreaSerializer
	queryset= Area.objects.all()
	lookup_field ='id'
# @login_required
class CargosViewSet(viewsets.ModelViewSet):
	serializer_class = CargoSerializer
	queryset = Cargo.objects.all()
	lookup_field ='id'