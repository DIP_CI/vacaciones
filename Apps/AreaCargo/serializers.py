# -*- encoding: utf-8 -*-

from rest_framework import serializers
from .models import *


class CargoSerializer(serializers.ModelSerializer):
	class Meta:
		model = Cargo
		fields =('id','cargo','fecha_registro','area')

class CargoSerializer1(serializers.ModelSerializer):
	class Meta:
		model = Cargo
		fields =('cargo','area')		

class AreaSerializer(serializers.ModelSerializer):
	cargos = CargoSerializer(many = True, read_only= True	)
	class Meta:
		model = Area
		fields = ('id', 'area', 'anio_creacion', 'cargos')
class AreaSerializer1(serializers.ModelSerializer):
	class Meta:
		model = Area
		fields = ('area', 'anio_creacion')

