# -*- encoding: utf-8 -*-

from django.db import models
import time

# Create your models here.
class Area(models.Model):

	area = models.CharField(max_length=200, verbose_name='Area')
	anio_creacion = models.CharField(max_length=4, verbose_name='Año Creacion', default=time.strftime("%Y"))

	class Meta:
		verbose_name = "AREA"
		verbose_name_plural = "Areas"
		ordering = ['-anio_creacion','area']

	def save(self, *args, **kwargs):
		self.area=self.area.upper()
		super(Area, self).save(*args, **kwargs)

	def __str__(self):
		return self.area

class Cargo(models.Model):

	cargo = models.CharField(max_length=50)
	area = models.ForeignKey(Area,related_name='cargos')
	fecha_registro = models.DateField(auto_now_add=True)

	class Meta:
		verbose_name = "CARGO"
		verbose_name_plural = "Cargos"
		ordering = ['cargo', 'area']

	def save(self, *args, **kwargs):
		self.cargo=self.cargo.upper()
		super(Cargo, self).save(*args, **kwargs)

	def __str__(self):
		return self.cargo + " (" + self.area.area + ") "