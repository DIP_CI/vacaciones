from django.conf.urls import patterns, include, url
from .views import *
from rest_framework.routers import DefaultRouter
from .views import *
# from .views import lista_integrantes,nuevo_integrante,modificar_integrante,eliminar_integrante


router = DefaultRouter()
router.register(r'get_areas', AreasViewSet)
router.register(r'get_cargos', CargosViewSet)


urlpatterns = patterns('Apps.AreaCargo.views',
    url(r'^$', index_area.as_view(), name='Area'),
    url(r'^', include(router.urls)),
)