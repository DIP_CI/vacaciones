# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Area',
            fields=[
                ('id', models.AutoField(primary_key=True, verbose_name='ID', auto_created=True, serialize=False)),
                ('area', models.CharField(verbose_name='Area', max_length=200)),
                ('anio_creacion', models.CharField(verbose_name='Año Creacion', default='2015', max_length=4)),
            ],
            options={
                'verbose_name': 'AREA',
                'verbose_name_plural': 'Areas',
                'ordering': ['-anio_creacion', 'area'],
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Cargo',
            fields=[
                ('id', models.AutoField(primary_key=True, verbose_name='ID', auto_created=True, serialize=False)),
                ('cargo', models.CharField(max_length=50)),
                ('fecha_registro', models.DateField(auto_now_add=True)),
                ('area', models.ForeignKey(related_name='cargos', to='AreaCargo.Area')),
            ],
            options={
                'verbose_name': 'CARGO',
                'verbose_name_plural': 'Cargos',
                'ordering': ['cargo', 'area'],
            },
            bases=(models.Model,),
        ),
    ]
