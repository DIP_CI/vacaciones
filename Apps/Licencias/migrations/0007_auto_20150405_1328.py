# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('Licencias', '0006_auto_20150327_0519'),
    ]

    operations = [
        migrations.AddField(
            model_name='licencia',
            name='autorizacion',
            field=models.CharField(max_length=200, null=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='licencia',
            name='detalles',
            field=models.CharField(max_length=500, null=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='licencia',
            name='periodo',
            field=models.CharField(max_length=50, null=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='licencia',
            name='registro_area_personal',
            field=models.CharField(max_length=10, null=True),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='licencia',
            name='fecha_retorno',
            field=models.DateField(default='2015-04-05', verbose_name='Retorno'),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='licencia',
            name='fecha_salida',
            field=models.DateField(default='2015-04-05', verbose_name='Salida'),
            preserve_default=True,
        ),
    ]
