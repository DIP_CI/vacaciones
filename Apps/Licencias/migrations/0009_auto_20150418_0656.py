# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('Licencias', '0008_auto_20150405_1436'),
    ]

    operations = [
        migrations.AlterField(
            model_name='licencia',
            name='fecha_retorno',
            field=models.DateField(default='2015-04-18', verbose_name='Retorno'),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='licencia',
            name='fecha_salida',
            field=models.DateField(default='2015-04-18', verbose_name='Salida'),
            preserve_default=True,
        ),
    ]
