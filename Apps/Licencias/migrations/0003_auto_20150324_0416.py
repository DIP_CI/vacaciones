# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('Licencias', '0002_auto_20150323_0019'),
    ]

    operations = [
        migrations.AlterField(
            model_name='licencia',
            name='fecha_retorno',
            field=models.DateField(verbose_name='Retorno', default='2015-03-24'),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='licencia',
            name='fecha_salida',
            field=models.DateField(verbose_name='Salida', default='2015-03-24'),
            preserve_default=True,
        ),
    ]
