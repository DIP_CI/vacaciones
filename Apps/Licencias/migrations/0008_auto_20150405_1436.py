# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('Licencias', '0007_auto_20150405_1328'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='licencia',
            name='solicitud',
        ),
        migrations.AddField(
            model_name='licencia',
            name='registro',
            field=models.CharField(max_length=200, verbose_name='registro', null=True, blank=True),
            preserve_default=True,
        ),
    ]
