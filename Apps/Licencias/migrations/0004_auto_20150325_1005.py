# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('Licencias', '0003_auto_20150324_0416'),
    ]

    operations = [
        migrations.AlterField(
            model_name='licencia',
            name='fecha_retorno',
            field=models.DateField(verbose_name='Retorno', default='2015-03-25'),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='licencia',
            name='fecha_salida',
            field=models.DateField(verbose_name='Salida', default='2015-03-25'),
            preserve_default=True,
        ),
    ]
