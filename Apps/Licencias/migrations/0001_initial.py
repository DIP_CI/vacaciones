# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import django.core.validators


class Migration(migrations.Migration):

    dependencies = [
        ('Trabajador', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='Licencia',
            fields=[
                ('id', models.AutoField(primary_key=True, verbose_name='ID', auto_created=True, serialize=False)),
                ('fecha_salida', models.DateField(verbose_name='Salida', default='2015-03-22')),
                ('fecha_retorno', models.DateField(verbose_name='Retorno', default='2015-03-22')),
                ('solicitud', models.CharField(verbose_name='solicitud', null=True, blank=True, max_length=200)),
                ('numero_horas', models.SmallIntegerField(verbose_name='Horas', default=0, validators=[django.core.validators.RegexValidator(code='Invalid number', message='solo se pueden pedir entre 0 a 8 horas de licencia', regex='^[0-8]$')])),
                ('numero_dias', models.SmallIntegerField(verbose_name='Dias', default=0, validators=[django.core.validators.RegexValidator(code='Invalid number', message='solo se pueden pedir entre 0 a 90 horas de licencia', regex='^[0-90]$')])),
                ('fecha_registro', models.DateField(verbose_name='Fecha Registro', auto_now_add=True)),
            ],
            options={
                'verbose_name': 'LICENCIA',
                'verbose_name_plural': 'Licencias',
                'ordering': ['-fecha_registro', 'trabajador', 'tipo_licencia'],
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Tipo_Licencia',
            fields=[
                ('id', models.AutoField(primary_key=True, verbose_name='ID', auto_created=True, serialize=False)),
                ('tipo_licencia', models.CharField(verbose_name='Tipo Licencia', max_length=200)),
            ],
            options={
                'verbose_name': 'TIPO LICENCIA',
                'verbose_name_plural': 'Tipos Licencias',
                'ordering': ['tipo_licencia'],
            },
            bases=(models.Model,),
        ),
        migrations.AddField(
            model_name='licencia',
            name='tipo_licencia',
            field=models.ForeignKey(related_name='tipo_licencias', to='Licencias.Tipo_Licencia', verbose_name='Licencia por'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='licencia',
            name='trabajador',
            field=models.ForeignKey(related_name='trabajadores', to='Trabajador.Trabajador', verbose_name='Trabajador'),
            preserve_default=True,
        ),
    ]
