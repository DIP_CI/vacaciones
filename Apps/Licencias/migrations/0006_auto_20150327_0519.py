# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('Licencias', '0005_auto_20150327_0233'),
    ]

    operations = [
        migrations.AlterField(
            model_name='licencia',
            name='numero_dias',
            field=models.SmallIntegerField(verbose_name='Dias', default=0),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='licencia',
            name='numero_horas',
            field=models.SmallIntegerField(verbose_name='Horas', default=0),
            preserve_default=True,
        ),
    ]
