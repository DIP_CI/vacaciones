# -*- encoding: utf-8 -*-

from django.db import models
import time
from django.core.validators import RegexValidator
from Apps.Trabajador.models import Trabajador, Vacaciones_Trabajador
# Create your models here.
class Tipo_Licencia(models.Model):

	tipo_licencia = models.CharField(max_length=200, verbose_name='Tipo Licencia')

	class Meta:
		verbose_name = "TIPO LICENCIA"
		verbose_name_plural = "Tipos Licencias"
		ordering = ['tipo_licencia',]

	def save(self, *args, **kwargs):
		self.tipo_licencia = self.tipo_licencia.upper()

		super(Tipo_Licencia, self).save(*args, **kwargs)

	def __str__(self):                             
		return self.tipo_licencia

class Licencia(models.Model):
	
	trabajador = models.ForeignKey(Trabajador, verbose_name='Trabajador' , related_name='trabajadores')
	tipo_licencia = models.ForeignKey(Tipo_Licencia, verbose_name='Licencia por',related_name='tipo_licencias')
	fecha_salida = models.DateField(default=time.strftime("%Y-%m-%d"), verbose_name='Salida')
	fecha_retorno = models.DateField(default=time.strftime("%Y-%m-%d"), verbose_name='Retorno')
	numero_horas = models.SmallIntegerField(default=0, verbose_name='Horas')
	numero_dias = models.SmallIntegerField(default=0, verbose_name='Dias')
	fecha_registro = models.DateField(auto_now_add=True, verbose_name='Fecha Registro')
	detalles = models.CharField(max_length=500, null=True)
	periodo =  models.CharField(max_length=50, null=True)
	autorizacion= models.CharField(max_length=200, null=True)
	registro = models.CharField(max_length=200, verbose_name='registro', null=True, blank=True)
	registro_area_personal= models.CharField(max_length=10, null=True)


	class Meta:
		verbose_name = "LICENCIA"
		verbose_name_plural = "Licencias"
		ordering = ['-fecha_registro','trabajador','tipo_licencia']
	def __str__(self):
		cadena = self.trabajador.dni + " -->" + str(self.fecha_salida) + " -  (horas : " + str(self.numero_horas) + ", dias : " + str(self.numero_dias) + ")"
		return  cadena
