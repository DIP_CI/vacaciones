# -*- encoding: utf-8 -*-

from rest_framework import serializers
from .models import *
# from apps.Trabajador.models import *
from Apps.Trabajador.serializers import *

class TLicenciaSerializer(serializers.ModelSerializer):
	class Meta:
		model = Tipo_Licencia
		fields =('id','tipo_licencia')

class TrabajadorSerializer(serializers.ModelSerializer):
	class Meta:
		model = Trabajador
		fields =('dni','nombres')	

class LicenciaSerializer(serializers.ModelSerializer):
	class Meta:
		model = Licencia
		fields = ('id', 'trabajador','tipo_licencia','fecha_salida', 'fecha_retorno', 'numero_horas','numero_dias','fecha_registro','detalles','periodo','autorizacion','registro','registro_area_personal')

class LicenciaSerializerTrabajador(serializers.ModelSerializer):
	class Meta:
		model = Licencia
		fields = ('id', 'trabajador','tipo_licencia','fecha_salida', 'fecha_retorno', 'numero_horas','numero_dias','fecha_registro')


