from django.shortcuts import render
from django.views.generic import CreateView
from django.core.urlresolvers import reverse_lazy
import json
from .models import *
from .serializers import *
from rest_framework import viewsets, generics, filters
from rest_framework.views import APIView
import codecs
from rest_framework import viewsets
from django.contrib.auth.decorators import login_required

# @login_required
def home(request):
	return render(request,'home.html')
# @login_required
class index_licencias(CreateView):
	template_name= 'licencia.html'
	model = Licencia
	success_url = reverse_lazy('Licencia')
# @login_required
class TipoLicenciaViewSet(viewsets.ModelViewSet):
	serializer_class = TLicenciaSerializer
	queryset= Tipo_Licencia.objects.all()
	lookup_field ='id'
# @login_required
class LicenciaViewSet(viewsets.ModelViewSet):
	serializer_class = LicenciaSerializer
	queryset = Licencia.objects.all()
	lookup_field ='id'