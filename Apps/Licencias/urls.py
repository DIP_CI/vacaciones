from django.conf.urls import patterns, include, url
from .views import *
from rest_framework.routers import DefaultRouter
from .views import *
from django.contrib.auth import authenticate,login
from django.contrib.auth.decorators import login_required
from django.contrib.auth import authenticate,login

router = DefaultRouter()
router.register(r'get_licencias', LicenciaViewSet)
router.register(r'get_tipo_licencia', TipoLicenciaViewSet)

urlpatterns = patterns('',
	url(r'^', include(router.urls)),
	url(r'licencia/', index_licencias.as_view(), name='Licencias'),
	
)