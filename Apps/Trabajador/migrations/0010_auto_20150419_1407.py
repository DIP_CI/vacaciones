# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('Trabajador', '0009_auto_20150419_1252'),
    ]

    operations = [
        migrations.AlterField(
            model_name='trabajadorareacargo',
            name='area',
            field=models.CharField(verbose_name='Area', max_length=60),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='trabajadorareacargo',
            name='cargo',
            field=models.CharField(max_length=50),
            preserve_default=True,
        ),
    ]
