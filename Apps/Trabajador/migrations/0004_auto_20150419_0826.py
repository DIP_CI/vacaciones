# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('Trabajador', '0003_auto_20150419_0825'),
    ]

    operations = [
        migrations.AlterField(
            model_name='trabajadorareacargo',
            name='dni',
            field=models.ForeignKey(to='Trabajador.Trabajador'),
            preserve_default=True,
        ),
    ]
