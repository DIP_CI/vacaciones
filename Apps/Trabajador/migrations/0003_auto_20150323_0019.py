# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('Trabajador', '0002_vacaciones_trabajador_horas_antes'),
    ]

    operations = [
        migrations.RenameField(
            model_name='vacaciones_trabajador',
            old_name='horas_antes',
            new_name='dias_horas',
        ),
    ]
