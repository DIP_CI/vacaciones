# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('Trabajador', '0002_auto_20150419_0820'),
    ]

    operations = [
        migrations.RenameField(
            model_name='trabajadorareacargo',
            old_name='anio',
            new_name='periodo',
        ),
    ]
