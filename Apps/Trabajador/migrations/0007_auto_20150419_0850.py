# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('Trabajador', '0006_auto_20150419_0833'),
    ]

    operations = [
        migrations.AlterField(
            model_name='trabajadorareacargo',
            name='periodo',
            field=models.SmallIntegerField(default=2000),
            preserve_default=True,
        ),
    ]
