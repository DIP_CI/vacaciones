# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('Trabajador', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='Anio',
            fields=[
                ('id', models.AutoField(primary_key=True, serialize=False, auto_created=True, verbose_name='ID')),
                ('anio', models.SmallIntegerField(default=2000)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='TrabajadorAreaCargo',
            fields=[
                ('id', models.AutoField(primary_key=True, serialize=False, auto_created=True, verbose_name='ID')),
                ('dni', models.CharField(max_length=8)),
                ('area', models.CharField(max_length=200, verbose_name='Area')),
                ('cargo', models.CharField(max_length=50)),
                ('anio', models.SmallIntegerField(default=2000)),
                ('fecha_registro', models.DateField(null=True, auto_now_add=True)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.AlterModelOptions(
            name='trabajador',
            options={'verbose_name_plural': 'Trabajadores', 'ordering': ['ap_paterno', 'ap_materno', 'nombres'], 'verbose_name': 'TRABAJADOR'},
        ),
        migrations.RemoveField(
            model_name='trabajador',
            name='area',
        ),
        migrations.RemoveField(
            model_name='trabajador',
            name='cargo',
        ),
        migrations.RemoveField(
            model_name='vacaciones_trabajador',
            name='dias_vacaciones_restantes',
        ),
        migrations.AddField(
            model_name='trabajador',
            name='foto',
            field=models.ImageField(upload_to='Trabajadores/fotos', null=True, verbose_name='Foto del trabajador'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='trabajador',
            name='situacion',
            field=models.CharField(max_length=50, null=True),
            preserve_default=True,
        ),
    ]
