# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('Trabajador', '0005_anio'),
    ]

    operations = [
        migrations.AddField(
            model_name='trabajador',
            name='situacion',
            field=models.CharField(max_length=50, null=True),
            preserve_default=True,
        ),
    ]
