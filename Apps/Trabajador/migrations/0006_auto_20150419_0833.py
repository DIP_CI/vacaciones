# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('Trabajador', '0005_auto_20150419_0827'),
    ]

    operations = [
        migrations.AlterField(
            model_name='trabajadorareacargo',
            name='id',
            field=models.CharField(max_length=10, serialize=False, primary_key=True),
            preserve_default=True,
        ),
    ]
