# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('Trabajador', '0004_auto_20150419_0826'),
    ]

    operations = [
        migrations.AlterField(
            model_name='trabajadorareacargo',
            name='area',
            field=models.ForeignKey(to='AreaCargo.Area'),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='trabajadorareacargo',
            name='cargo',
            field=models.ForeignKey(to='AreaCargo.Cargo'),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='trabajadorareacargo',
            name='periodo',
            field=models.ForeignKey(to='Trabajador.Anio'),
            preserve_default=True,
        ),
    ]
