# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('Trabajador', '0008_auto_20150419_1124'),
    ]

    operations = [
        migrations.AlterField(
            model_name='trabajadorareacargo',
            name='id',
            field=models.AutoField(verbose_name='ID', primary_key=True, auto_created=True, serialize=False),
            preserve_default=True,
        ),
    ]
