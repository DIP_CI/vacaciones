# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import django.core.validators


class Migration(migrations.Migration):

    dependencies = [
        ('AreaCargo', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='Trabajador',
            fields=[
                ('dni', models.CharField(primary_key=True, max_length=8, serialize=False, validators=[django.core.validators.RegexValidator(message='el DNI contiene 8 digitos', regex='^\\d{8}$')])),
                ('nombres', models.CharField(max_length=50)),
                ('ap_paterno', models.CharField(max_length=50)),
                ('ap_materno', models.CharField(max_length=50)),
                ('email', models.EmailField(blank=True, null=True, max_length=75)),
                ('direccion', models.CharField(blank=True, null=True, max_length=300)),
                ('telf_fijo', models.CharField(null=True, default='000000000000', max_length=12)),
                ('telf_movil', models.CharField(null=True, default='999999999999', max_length=12)),
                ('dias_vacaciones', models.SmallIntegerField(default=30)),
                ('area', models.ForeignKey(to='AreaCargo.Area')),
                ('cargo', models.ForeignKey(to='AreaCargo.Cargo')),
            ],
            options={
                'verbose_name': 'TRABAJADOR',
                'verbose_name_plural': 'Trabajadores',
                'ordering': ['ap_paterno', 'ap_materno', 'nombres', 'cargo'],
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Vacaciones_Trabajador',
            fields=[
                ('id', models.AutoField(primary_key=True, verbose_name='ID', auto_created=True, serialize=False)),
                ('dias_vacaciones', models.SmallIntegerField(default=30)),
                ('dias_acumulados', models.SmallIntegerField(default=0)),
                ('horas_acumuladas', models.SmallIntegerField(default=0)),
                ('dias_vacaciones_restantes', models.SmallIntegerField(default=30)),
                ('anio', models.SmallIntegerField(null=True, default=0)),
                ('fecha_registro', models.DateField(auto_now_add=True)),
                ('trabajador', models.ForeignKey(to='Trabajador.Trabajador')),
            ],
            options={
                'verbose_name': 'VACACIONES POR TRABAJADOR',
                'verbose_name_plural': 'Vacaciones por Trabajador',
                'ordering': ['trabajador__dni', 'fecha_registro'],
            },
            bases=(models.Model,),
        ),
    ]
