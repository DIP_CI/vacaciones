# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('Trabajador', '0007_auto_20150419_0850'),
    ]

    operations = [
        migrations.RenameField(
            model_name='trabajador',
            old_name='telf_fijo',
            new_name='telefono',
        ),
        migrations.RemoveField(
            model_name='trabajador',
            name='telf_movil',
        ),
        migrations.AlterField(
            model_name='trabajador',
            name='situacion',
            field=models.CharField(verbose_name='Situacion', choices=[('PERMANENTE', 'PERMANENTE'), ('NOMBRADO', 'NOMBRADO'), ('REPUESTO JUDICIAL', 'REPUESTO JUDICIAL'), ('OBRERO NOMBRADO', 'OBRERO NOMBRADO'), ('CAS', 'CAS')], max_length=50),
            preserve_default=True,
        ),
    ]
