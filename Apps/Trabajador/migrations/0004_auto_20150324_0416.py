# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('Trabajador', '0003_auto_20150323_0019'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='vacaciones_trabajador',
            name='dias_horas',
        ),
        migrations.RemoveField(
            model_name='vacaciones_trabajador',
            name='dias_vacaciones_restantes',
        ),
    ]
