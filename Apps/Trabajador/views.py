from django.shortcuts import render
from django.views.generic import TemplateView, CreateView, ListView, UpdateView
from django.core.urlresolvers import reverse_lazy
from django.http import HttpResponse
from braces import views
import json
from .models import *
from .serializers import *
from rest_framework import viewsets, generics, filters
from django.http import Http404
from rest_framework.views import APIView
from rest_framework.decorators import api_view
from rest_framework.response import Response
from rest_framework import status
import codecs
from rest_framework import viewsets
from django.contrib.auth.decorators import login_required
# from rest_framework import permissions

# Create your views here.
# @login_required
class index_trabajador(CreateView):
	template_name= 'index_trabajador.html'
	model = Trabajador
	success_url = reverse_lazy('Trabajador')

class prueba(CreateView):
	template_name= 'prueba.html'
	model = Trabajador
	success_url = reverse_lazy('Trabajador')
	# permission_classes =(permissions.AllowAny);
# @login_required
class reporte_trabajador(CreateView):
	template_name= 'reportes.html'
	model = Trabajador
	success_url = reverse_lazy('Trabajador')
	# permission_classes =(permissions.AllowAny);
# @login_required
class nuevo_trabajador(CreateView):
	template_name= 'nuevo_trabajador.html'
	model = Trabajador
	success_url = reverse_lazy('nuevo_trabajador')
	# permission_classes =(permissions.AllowAny);
# @login_required
class TrabajadoresViewSet(viewsets.ModelViewSet):
	serializer_class = TrabajadorSerializer
	queryset = Trabajador.objects.all()
	lookup_field ='dni'
	# permission_classes =(permissions.AllowAny);

class TrabajadorAreaCargoViewSet(viewsets.ModelViewSet):
	serializer_class =  TrabajadorAreaCargoSerializer
	queryset = TrabajadorAreaCargo.objects.all()
	# lookup_field ='id'

# @login_required
class VacacionesTrabajadorViewSet(viewsets.ModelViewSet):
	serializer_class = VacacionesSerializer
	queryset=Vacaciones_Trabajador.objects.all()
	lookup_field='id'
# @login_required
class VacacionesTrabajadorReporteViewSet(viewsets.ModelViewSet):
	serializer_class = VacacionesTrabajadorSerializer
	queryset=Vacaciones_Trabajador.objects.all()
	lookup_field='id'
	# filter_fields=('trabajador','anio')

class AnioViewSet(viewsets.ModelViewSet):
	serializer_class = AnioSerializer
	model = Anio
	queryset = Anio.objects.all()
	lookup_field='id'

class PeriodoIndex(CreateView):
	template_name= 'periodos.html'
	model = Anio
	success_url = reverse_lazy('periodos')

class Anios_Vacaciones_ViewSet(viewsets.ModelViewSet):
	serializer_class = VacacionesSerializer
	queryset = Vacaciones_Trabajador.objects.all()
	lookup_field='trabajador'