from django.conf.urls import patterns, include, url
from .views import *
from rest_framework.routers import DefaultRouter

router = DefaultRouter()
router.register(r'get_trabajadores', TrabajadoresViewSet)
router.register(r'get_vacaciones', VacacionesTrabajadorViewSet)
router.register(r'get_trabajador_area_cargo', TrabajadorAreaCargoViewSet)
router.register(r'get_vacaciones_trabajador', VacacionesTrabajadorReporteViewSet)
router.register(r'get_anios_vacaciones',  Anios_Vacaciones_ViewSet)
router.register(r'get_anios', AnioViewSet)

urlpatterns = patterns('',
	url(r'^', include(router.urls)),
	url(r'^lista_trabajadores/', index_trabajador.as_view(), name='Trabajador'),
	url(r'^prueba/', prueba.as_view(), name='prueba'),
	url(r'^reportes/', reporte_trabajador.as_view(), name="reportes"),
	url(r'^periodos/', PeriodoIndex.as_view(), name="periodos"),
)