# -*- encoding: utf-8 -*-

from rest_framework import serializers
from .models import *
from Apps.AreaCargo.models import *

class CargoSerializer(serializers.ModelSerializer):
	class Meta:
		model = Cargo
		fields =('id','cargo','area')		

class AreaSerializer(serializers.ModelSerializer):
	class Meta:
		model = Area
		fields = ('id', 'area', 'anio_creacion')

class AnioSerializer(serializers.ModelSerializer):
	class Meta:
		model = Anio
		fields = ('id','anio')

class TrabajadorSerializer(serializers.ModelSerializer):
	class Meta:
		# cargo = CargoSerializer(many = True, read_only=True)
		# area = 	AreaSerializer(many = True, read_only=True)
		model = Trabajador
		fields =('dni','nombres','ap_paterno','ap_materno','email','direccion','telefono','dias_vacaciones', 'situacion','foto')

class TrabajadorAreaCargoSerializer(serializers.ModelSerializer):
	class Meta:
		cargo = CargoSerializer(many = True, read_only=True)
		area = 	AreaSerializer(many = True, read_only=True)
		model = TrabajadorAreaCargo
		fields =('dni','area','cargo','periodo','fecha_registro')

class VacacionesSerializer(serializers.ModelSerializer):
	class Meta:
		model = Vacaciones_Trabajador
		fields = ('id', 'trabajador', 'dias_vacaciones','dias_acumulados','horas_acumuladas','anio')

class VacacionesTrabajadorSerializer(serializers.ModelSerializer):
	trabajador = TrabajadorSerializer(read_only=True)
	class Meta:
		model = Vacaciones_Trabajador
		fields = ('id', 'trabajador', 'dias_vacaciones','dias_acumulados','horas_acumuladas','anio')

