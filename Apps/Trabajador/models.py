# -*- encoding: utf-8 -*-

from django.db import models
from django.core.validators import RegexValidator

from Apps.AreaCargo.models import Area, Cargo

# Create your models here.
class Trabajador(models.Model):
	dni = models.CharField(primary_key=True, max_length=8, validators=[
			RegexValidator(
				regex='^\d{8}$',
				message='el DNI contiene 8 digitos',
			)]
		)
	nombres = models.CharField(max_length=50)
	ap_paterno = models.CharField(max_length=50)
	ap_materno = models.CharField(max_length=50)
	email = models.EmailField(null=True, blank=True)
	direccion = models.CharField(max_length=300, null=True, blank=True)
	telefono = models.CharField(max_length=12, default="000000000000", null=True)
	dias_vacaciones = models.SmallIntegerField(default=30)
	situaciones = (
        ('PERMANENTE', 'PERMANENTE'),
        ('NOMBRADO', 'NOMBRADO'),
        ('REPUESTO JUDICIAL', 'REPUESTO JUDICIAL'),
        ('OBRERO NOMBRADO', 'OBRERO NOMBRADO'),
        ('CAS', 'CAS'),
    )
	situacion = models.CharField('Situacion',max_length=50,choices=situaciones)
	foto = models.ImageField('Foto del trabajador', upload_to='Trabajadores/fotos', null=True)


	class Meta:
		verbose_name = "TRABAJADOR"
		verbose_name_plural = "Trabajadores"
		ordering = ['ap_paterno','ap_materno','nombres']

	def __str__(self):
		valor = self.ap_paterno + " " + self.ap_materno + ", " + self.nombres + " --> ("+ self.dni +")"
		return  valor

	def save(self, *args, **kwargs):
		self.nombres=self.nombres.upper()
		self.ap_paterno=self.ap_paterno.upper()
		self.ap_materno=self.ap_materno.upper()
		self.direccion=self.direccion.upper()
		self.telefono=self.telefono.upper()
		super(Trabajador, self).save(*args, **kwargs)


class Vacaciones_Trabajador(models.Model):
	trabajador = models.ForeignKey(Trabajador)
	dias_vacaciones = models.SmallIntegerField(default=30)
	dias_acumulados = models.SmallIntegerField(default=0)
	horas_acumuladas = models.SmallIntegerField(default=0)
	anio = models.SmallIntegerField(default=0,null=True)
	fecha_registro = models.DateField(auto_now_add=True)

	class Meta:
		verbose_name = "VACACIONES POR TRABAJADOR"
		verbose_name_plural = "Vacaciones por Trabajador"
		ordering = ['trabajador__dni','fecha_registro']

	def save(self, *args, **kwargs):
		# self.dias_vacaciones=self.cargo.upper()
		super(Vacaciones_Trabajador, self).save(*args, **kwargs)

	def __str__(self):
		cadena = self.trabajador.dni + " - "  + str(self.dias_vacaciones)
		return cadena

class Anio(models.Model):
	anio = models.SmallIntegerField(default=2000)

	def __str__(self):
		cadena = str(self.anio)
		return cadena

class TrabajadorAreaCargo(models.Model):
	dni = models.ForeignKey(Trabajador)
	area = area = models.CharField(max_length=60, verbose_name='Area')
	cargo = cargo = models.CharField(max_length=50)
	periodo = models.SmallIntegerField(default=2000)
	fecha_registro = models.DateField(auto_now_add=True, null=True)

	def __str__(self):
		valor = self.dni + " " + self.area+ " " + self.cargo
		return  valor