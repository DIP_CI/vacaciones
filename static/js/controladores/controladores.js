var app = angular.module('App', []);

app.config(function($interpolateProvider, $httpProvider){
	$interpolateProvider.startSymbol('[[');
	$interpolateProvider.endSymbol(']]');
	$httpProvider.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest';
	$httpProvider.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded';
	$httpProvider.defaults.xsrfCookieName = 'csrftoken';
    $httpProvider.defaults.xsrfHeaderName = 'X-CSRFToken';
});
app.directive('datepicker', function() {
    return {
        restrict: 'A',
        require : 'ngModel',
        link : function (scope, element, attrs, ngModelCtrl) {
            $(function(){
                element.datepicker({
                    dateFormat:'yy-mm-dd',
                    onSelect:function (date) {
                        ngModelCtrl.$setViewValue(date);
                        scope.$apply();
                    }
                });
            });
        }
    }
});

app.filter("entero",function(){    
    return function(val){
        return Math.floor(val);
    }
});
app.filter("toUpper", function(){
    return function(text){
        if(text != null){
            return text.toUpperCase();
        }
    }
})

app.controller('controlador', function($scope, $http){
	var fecha = new Date();
	var ano = fecha.getFullYear();
	$scope.date=fecha.toJSON().slice(0,10);
	$scope.areas=[];
	$scope.trabajadores=[];
	$scope.cargos=[];
	$scope.selected=false;
	$scope.ListaCargos=[];
	$scope.ListaAreas=[];
	$scope.trabajador =[];
	$scope.usuarios=[];
	$scope.LicenciaTrabajador={};
	$scope.vacacionesTrabajadores={};
	$scope.dt={};
	$scope.vac_trabajador={};
	$scope.id_area="";
	$scope.reporte=false;
	$scope.anios="";
	$scope.tipo_licencia="";
	$scope.periodos_trabajador={};
	$scope.bool=false;
	$scope.area_seleccionada="";
	$scope.cargoseleccionado="";
	$scope.periodos={};
	 LLamarAreas();
     LLamarTrabajadores();
     ListarAnios();

     $scope.LimpiarArea = function(){
     	$scope.area="";
     }
     function LLamarAreas(){
     	$http.get('/areas/get_areas/')
     	.success(function(data) {
			$scope.areas = data;
	    })
	    .error(function(){
	    	alertify.error('Error en los datos, verifiquelos!')
	     });
     }

     $scope.LlamarTrabajador=function(id){
     	// $scope.licencia.periodo="";
     	$http.get('/trabajadores/get_trabajadores/'+id)
     	.success(function(data) {
     		
			$scope.trabDetalle = data;
			$scope.periodos_trabajador={};
			// MostrarArea(data.area);
			// MostrarCargo(data.cargo);
			Periodos_Trabajador(id);
	    })
	    .error(function(){
	    	if(id!=null){
	    		alertify.error('El Trabajador no existe!!');
	    		$scope.trabDetalle="";
	    		$scope.licencia="";	
	    	}
	     });
     }  
     function MostrarArea (id){
	    $http.get('/areas/get_areas/'+id)
		.success(function(data) {
			$scope.areaTrabajador=data.area;
	     })
	     .error(function(){
	      	alertify.error('Error en los datos, verifiquelos!')
	     });
     }
     function MostrarCargo(id){
	    $http.get('/areas/get_cargos/'+id)
		.success(function(data) {
			$scope.cargoTrabajador=data.cargo;
	     })
	     .error(function(){
	      	alertify.error('Error en los datos, verifiquelos!')
	     });
     }
     function LLamarTrabajadores(){
     	$http.get('/trabajadores/get_trabajadores/')
     	.success(function(data) {
			$scope.trabajadores = data;
	    })
	    .error(function(e){
	    	alertify.error('Error en los datos, verifiquelos!')
	    	console.log(e);
	     });
     } 

     $scope.ListarCargos = function(id) {		
		$http.get('/areas/get_areas/'+id)
		.success(function(data) {
			$scope.ListaCargos=data.cargos;
			$scope.cargos=data.cargos;
			$scope.id_area=id;
        })
        .error(function(){
        	alertify.error('Error en los datos, verifiquelos!')
     });
	}	

	$scope.SeleccionArea= function(data){
		$scope.area_seleccionada=data;
	}

	function ListarAnios(){
		$http.get('/trabajadores/get_anios/')
		.success(function(data) {
			$scope.anios=data;
        })
        .error(function(){
        	alertify.error('Error en los datos, verifiquelos!')
     });
	}

    $scope.seleccionar = function(area){
    	$scope.area=area.area;
    	alert(area.area);
    }  

    $scope.GuardarArea = function(data){
    	var fecha = new Date();
		var anio = fecha.getUTCFullYear();
		console.log(data);
    	var datos = {
    		area:data.area,
    		anio_creacion:anio
    	}
    	$http({
    		method:'POST',
    		url:'/areas/get_areas/',
    		data:datos,
    		headers: {'Content-Type': 'application/json'}
    	})
		.success(function(){
			LLamarAreas();
			alertify.success('El area ' + data.area+' se agrego correctamente');
			$scope.area="";
		 })
		.error(function(e){
		   alertify.error('Error en los datos!');
		 })
	}

	$scope.GuardarPeriodo = function(anio){
		var existe=false;
		var datos = {
    		anio:anio
    	}
    	ListarAnios();
    	for (var i = 0; i < $scope.anios.length; i++) {
    		if($scope.anios[i].anio==anio){
    			existe=true;
    		}
    	};
    	console.log(existe);
    	if(!existe){
    		$http({
    		method:'POST',
    		url:'/trabajadores/get_anios/',
    		data:datos,
	    		headers: {'Content-Type': 'application/json'}
	    	})
			.success(function(){
				alertify.success('<i class="fa fa-check"></i> El periodo ' + anio+' se agrego correctamente');
				$scope.anio="";
				ListarAnios();
			 })
			.error(function(e){
			   alertify.error('Error en los datos!!');
			   console.log(e);
			 })
    	}
    	else{
    		 alertify.error('<i class="fa fa-close"></i> El periodo ya existe. Por favor digite otro!!');
    	}
	}

	$scope.DetalleTrabajador = function(id){
		$http.get('/trabajadores/get_trabajadores/'+id)
     	.success(function(data) {
			$scope.trabDetalle = data;
			// MostrarArea(data.area);
			// MostrarCargo(data.cargo);
			 ListarAreaCargo(id);
	    })
	    .error(function(){
	    	alertify.error('Error en los datos, verifiquelos!')
	     });
	}

	$scope.Detalle_Licencia_individual = function(dni,periodo){
		$scope.LicenciaTrabajador="";
		if(dni==null){
			alertify.error('DNI Incorrecto!!');
		}
		else{
				$scope.reporte=true;
				$http.get('/licencias/get_licencias/')
				.success(function(data){
					var licencias =[];
					var horas=0;
					var dias=0;
					var j=0;

					for (var i = 0; i < data.length; i++) {
						if(data[i].trabajador==dni && data[i].periodo==periodo){
							licencias[j]=data[i];
							j++;
						}
					}
					if(licencias.length!=0){
						$scope.LicenciaTrabajador=licencias;
						Vacaciones_Trabajador(dni,periodo);
						licencias ="";
						$scope.LlamarTrabajador(dni);
						$scope.vac_trabajador="";
					}
				})
				.error(function(e){
					alertify.error('Error en los datos, verifiquelos!');
				})	
		}
	}

	function Vacaciones_Trabajador(dni, periodo){
		$scope.vac_trabajador="";
		$http.get('/trabajadores/get_vacaciones/')
		.success(function(data){
			for (var i = 0; i < data.length; i++) {
					if(data[i].trabajador==dni && data[i].anio==periodo){
						$scope.vac_trabajador=data[i];
						i = data.length;			
					}
					else{
						console.log('no se encontro')
					}
			};
		})
		.error(function(e){
			alertify.error('Error en los datos, verifiquelosSSSS!')
		})	
	}
	function Periodos_Trabajador(dni){
		$http.get('/trabajadores/get_vacaciones/')
		.success(function(data){	
			for (var i = 0; i < data.length; i++) {
					if(data[i].trabajador==dni){
						$scope.periodos_trabajador[i]=data[i].anio;
					}
			};
			console.log('estos son los periodos:');
			console.log($scope.periodos_trabajador);
		})
		.error(function(){
			alertify.error('El trabajador no tiene vacaciones creadas!!');
		})
	}
	function Existe_Periodo(periodo,dni){
		$http.get('/trabajadores/get_vacaciones/')
		.success(function(data){	
			for (var i = 0; i < data.length; i++) {
					if(data[i].trabajador==dni.dni){
						$scope.bool=true;
						console.log('si entra')
					}
			}
		})
		.error(function(){
			alertify.error('El trabajador no tiene vacaciones creadas!!');
		})
	}
	function validarSiNumero(numero){
	    if (!/^([0-9])*$/.test(numero))
	      {
	      	return false;
	      }
	      else{
	      	return true;
	      }
	  }

	$scope.InsertarTrabajador = function(data,area,cargo){
		if(data.dni==null || data.nombres==null || data.ap_paterno==null || data.ap_materno==null){
			alertify.error('ERROR! por favor Verifique bien los datos!!!!');
			$scope.trabajador="";
		}
		else{
			if(validarSiNumero(data.dni)){
				if (data.dni.length==8) {
					var areas = $scope.areas;
			    	var trabajador = {
			    		dni:data.dni,
			    		nombres:data.nombres,
			    		ap_paterno:data.ap_paterno,
			    		ap_materno:data.ap_materno,
			    		direccion:data.direccion,
			    		telf_movil:data.telf_movil,
			    		dias_vacaciones:data.dias_vacaciones,
			    		// area:area.id,
			    		// cargo:cargo.id,
			    		situacion:data.situacion
			    	}
			    	var datos={
			    		trabajador:data.dni,
			    		anio:ano
			    	}
			    	$http({
			    		method:'POST',
			    		url:'/trabajadores/get_trabajadores/',
			    		data:trabajador,
			    		headers: {'Content-Type': 'application/json'}
			    	})
					.success(function(){
						LLamarTrabajadores();
						LLamarAreas();
						alertify.success('El trabajador ' + data.nombres+' se agrego correctamente al sistema');
						$scope.trabajador = "";
					 })
					.error(function(e){
						alertify.error('A ocurrido un ERROR! por favor Verifique bien los datos!!!!');
					   	console.log('ha ocurrido un fallo',e);	
					 })
				}
				else{
					alertify.error('El DNI debe contener 8 caracteres!!!');
					$scope.trabajador="";
				}	
			}
			else{
				alertify.error('El DNI debe contener solo caracteres numericos');
				$scope.trabajador="";
			}
		}
	}

	$scope.GenerarVacaciones= function(anio,dni){
	   	var datos={
    		trabajador:dni.dni,
    		anio:anio.anio
    	}
    	$http.get('/trabajadores/get_vacaciones/')
		.success(function(data){	
			for (var i = 0; i < data.length; i++) {
					if(data[i].trabajador==dni.dni && data[i].anio == anio.anio){
						console.log('si entra');
						$scope.bool=true;
					}
			}
			if($scope.bool==true){
	    		alertify.error('El trabajador ya tiene vacaciones creadas para este periodo! elija otro');
	    		console.log('no hay');
	    		$scope.bool=false;
	    	}
	    	else{
	    		$http({
			    method:'POST',
			    url:'/trabajadores/get_vacaciones/',
			    data:datos,
			 	headers: {'Content-Type': 'application/json'}
				 })
				.success(function(){
					alertify.success('Vacaciones creadas correctamente');
				})
				.error(function(e){
					console.log('ha ocurrido un fallo en la vacaciones',e);	
				});
	    	}
		})
		.error(function(){
			alertify.error('El trabajador no tiene vacaciones creadas!!');
		})
	}

	$scope.AgregarAreaCargo = function(dni,periodo,area,cargo){
		var data={
			dni:dni[0].toString(),
			area:area[0].toString(),
			cargo:cargo[0].toString(),
			periodo:periodo[0].toString()
		}
		console.log(data)
		$http({
			    method:'POST',
			    url:'/trabajadores/get_trabajador_area_cargo/',
			    data:data,
			    headers: {'Content-Type': 'application/json'}
			 })
			.success(function(){
				alertify.success('<i class="fa fa-check"></i> Datos agregados correctamente!');
			 })
			.error(function(e){
			   // alertify.warning('<i class="fa fa-close"></i> Error al agregar los datos! por favor vuelvalo a intentar');
			   console.log(e);
			 })
	}

	function ListarAreaCargo(dni){
		$scope.puestos={};
		$http.get('/trabajadores/get_trabajador_area_cargo/')
		.success(function(data){
			for (var i = 0; i < data.length; i++) {
				if(data[i].dni==dni){
					$scope.puestos[i]=data[i];
				}
			};
			console.log($scope.puestos)
		})
		.error(function(e){
			console.log(e);
		})

	}

	 $scope.InsertarCargo = function(data,carguito){
	  		var datos={
		  		area:  data.toString(),
		  		cargo: carguito
		  	}
		  	console.log(datos);
		  	$http({
			    method:'POST',
			    url:'/areas/get_cargos/',
			    data:datos,
			    headers: {'Content-Type': 'application/json'}
			 })
			.success(function(){
			   alertify.success('El cargo se agrego correctamente');
			   $scope.carguito="";
			 })
			.error(function(e){
			   alertify.warning('Error al agregar cargo!! por favor vuelvalo a intentar');
			 })
	}

	$scope.EliminarArea = function(id, area){
		alertify.confirm('¿Desea Eliminar el elemento?', function(e){
    		if(e){  
					$http({
			    		method:'DELETE',
			    		url:'/areas/get_areas/'+id,
			    		headers: {'Content-Type': 'application/json'}
			    	})
					.success(function(){
					   LLamarAreas();
					   alertify.success('Se elimino el elemento: '+area);
					 })
					.error(function(e){
					   console.log('ha ocurrido un fallo',e);	
					 })
    		}
    		else{
    			alertify.alert('¿Desea cancelar la operación?');
    		}
    	});
	}
	$scope.EliminarPeriodo = function(id){
		alertify.confirm('¿Desea Eliminar el elemento?', function(e){
    		if(e){  
					$http({
			    		method:'DELETE',
			    		url:'/trabajadores/get_anios/'+id,
			    		headers: {'Content-Type': 'application/json'}
			    	})
					.success(function(){
					   ListarAnios();
					   alertify.success('Se elimino el elemento correctamente! ');
					 })
					.error(function(e){
					   console.log('ha ocurrido un fallo',e);	
					 })
    		}
    		else{
    			alertify.alert('¿Desea cancelar la operación?');
    		}
    	});
	}
	$scope.LimpiarPeriodo = function(){
		$scope.anio="";
	}

	$scope.EliminarCargo = function(id){
		alertify.confirm('¿Desea Eliminar el elemento?', function(e){

    		if(e){  
					$http({
			    		method:'DELETE',
			    		url:'/areas/get_cargos/'+id,
			    		headers: {'Content-Type': 'application/json'}
			    	})
					.success(function(){
					   alertify.success('Se elimino el elemento correctamente!');
					   $scope.ListarCargos($scope.id_area);
					 })
					.error(function(e){
					   alertify.error('Ha ocurrido un ERROR!, por favor vuelva a intentarlo!',e);	
					 })
    		}
    		else{
    			alertify.alert('¿Desea cancelar la operación?');
    		}
    	});
	}

	$scope.EliminarTrabajador = function(id){
		console.log(id);
		alertify.confirm('¿Desea Eliminar el elemento?', function(e){
    		if(e){  
					$http({
			    		method:'DELETE',
			    		url:'/trabajadores/get_trabajadores/'+id,
			    		headers: {'Content-Type': 'application/json'}
			    	})
					.success(function(){
					   alertify.success('Se elimino el elemento');
					   LLamarTrabajadores();
					 })
					.error(function(e){
					   console.log('ha ocurrido un fallo',e);	
					 })
    		}
    		else{
    			alertify.alert('¿Desea cancelar la operación?');
    		}
    	});
	}

	$scope.limpiar_licencia= function(){
		$scope.licencia="";
		$scope.licencia.periodo="";
		$scope.trabDetalle="";
	}

	$scope.InsertarLicencia= function(data){
		var licencia={};
		var datos_vacaciones={};
		var horas =0;
		var dias =0;
		var id_vacaciones="";
		var dias_totales=0;
		var mono=data;
		var vacaciones_anio={};
		if(data.tipo=='licencia1'){
				licencia = {
		    		trabajador:data.dni,
		    		tipo_licencia:1,
		    		fecha_salida:data.fecha_salida,
		    		fecha_retorno:data.fecha_salida,
		    		numero_horas:data.tiempo,
		    		numero_dias:0,
		    		fecha_registro:fecha.toJSON().slice(0,10),
		    		detalles:data.detalles,
		    		periodo:data.periodo,
		    		autorizacion:data.autorizacion,
		    		registro:data.registro,
		    		registro_area_personal:data.registro_area_personal
	    	}
	    	$scope.tipo_licencia="LICENCIA PARTICULAR";

		}
		else{
			if((Date.parse(data.fecha_salida)) > (Date.parse(data.fecha_retorno))){
				alertify.error('La fecha salida no puede ser mayor que la fecha de retorno');
				$scope.licencia="";
			}
			else{
				licencia = {
			    	trabajador:data.dni,
		    		tipo_licencia:2,
		    		fecha_salida:data.fecha_salida,
		    		fecha_retorno:data.fecha_retorno,
		    		numero_horas:0,
		    		numero_dias:data.tiempo,
		    		fecha_registro:fecha.toJSON().slice(0,10),
		    		detalles:data.detalles,
		    		periodo:data.periodo,
		    		autorizacion:data.autorizacion,
		    		registro:data.registro,
		    		registro_area_personal:data.registro_area_personal
		    	}
		    	$scope.tipo_licencia="LICENCIA A CUENTA DE VACACIONES";
		    }
		}

		if (licencia.trabajador ==null || licencia.fecha_salida==null || 
			licencia.fecha_retorno==null || licencia.tipo_licencia==null || licencia.periodo==null || licencia.registro==null || licencia.numero_dias==null 
			|| licencia.numero_horas==null) {
			console.log(licencia);
			alertify.error('Datos incorrectos! Vefirique los datos!!!');
			$scope.licencia="";
		}
		else{
		    	$http({
		    		method:'POST',
		    		url:'/licencias/get_licencias/',
		    		data:licencia,
		    		headers: {'Content-Type': 'application/json'}
		    	})
				.success(function(){
					var nuevos_dias_acumulados=0;
					var nuevas_horas_acumuladas=0;
					var nuevo_dias_restantes=0;
					var dias_horas=0;
					$http.get('/trabajadores/get_vacaciones/')
					.success(function(vacaciones){
						for (var i = 0; i < vacaciones.length; i++) {
							if(vacaciones[i].trabajador==licencia.trabajador && vacaciones[i].anio==licencia.periodo){
								vacaciones_anio=vacaciones[i];
								i=vacaciones.length;
							}
						};
						if(licencia.tipo_licencia==1){
				     		nuevas_horas_acumuladas=vacaciones_anio.horas_acumuladas+ parseInt(licencia.numero_horas);
					     		var nuevas_vacaciones={
								trabajador:mono.dni,
								dias_acumulados:vacaciones_anio.dias_acumulados,
								horas_acumuladas:nuevas_horas_acumuladas
							}
				     	}
				     	else{
				     		nuevos_dias_acumulados=vacaciones_anio.dias_acumulados+ parseInt(licencia.numero_dias);
				     		nuevas_horas_acumuladas=vacaciones_anio.horas_acumuladas;

					     		var nuevas_vacaciones={
								trabajador:mono.dni,
								dias_acumulados:nuevos_dias_acumulados,
								horas_acumuladas:vacaciones_anio.horas_acumuladas
							}	
				     	}
				     	console.log(nuevas_vacaciones);
				     				$http({
							    		method:'PUT',
							    		url:'/trabajadores/get_vacaciones/'+vacaciones_anio.id,
							    		data:nuevas_vacaciones,
							    		headers: {'Content-Type': 'application/json'}
							    	})
									.success(function(){
										alertify.confirm("¿Desea imprimir la licencia?", function (e) {
										    if (e) {
										        pdfLicencia();
										        $scope.licencia = "";
										    } else {
										        $scope.licencia = "";
										    }
										});
										
									 })
									.error(function(e){
										alertify.success('A ocurrido un error, verifique los datos :(sssssss!!');{}
									   	console.log('ha ocurrido un fallo',e);	
									  	 $scope.licencia="";
									 })
					})
					.error(function(e){
						console.log(e)
					})
				 })
				.error(function(e){
					alertify.success('A ocurrido un error, verifique los datos :(!!');
				   console.log('ha ocurrido un error, verifique los datos!!',e);	
				   $scope.licencia="";
				 });
		}
	}

	$scope.Dias_Trabajador= function(){
		var dias=0;
		var dias_totales=0;
		var horas=0;
	     	$http.get('/licencias/get_licencias/')
		     	.success(function(data) {
		     	for(var i = 0; i<data.length; i++){
		     		horas=horas+parseInt(data[i].numero_horas);
		     		dias=dias+parseInt(data[i].numero_dias);
		     	}
		     	dias_totales=dias+Math.round((horas/8));
				$scope.dt.dias=dias;
				$scope.dt.dias_horas=Math.round((horas/8));
				$scope.dt.total_dias=dias_totales;
				$scope.dt.dias_falta=30-dias_totales;
		    })
		    .error(function(){
		    	alertify.error('El Trabajador no existe!!');
		    	$scope.trabDetalle="";
		    	$scope.licencia="";
		     });
	}

	$scope.llamarLicencias= function(id){
		var dias=0;
		var dias_totales=0;
		var horas=0;
	     	$http.get('/licencias/get_vacaciones/')
	     	.success(function(data) {
	     		
		    })
		    .error(function(){
		    	alertify.error('El Trabajador no existe!!');
		     });
	}

	$scope.listar_Licencias = function(){
		$http.get('/trabajadores/get_vacaciones_trabajador')
		.success(function(data){
			$scope.vacacionesTrabajadores=data;
		})
		.error(function(e){
			console.log('error:'+e)
		});
	}
});

	function pdfGeneral(){
		var doc = new jsPDF();
		var specialElementHandlers = {
		'#editor': function(element, renderer){
			return true;
		}};
		doc.fromHTML($('#reporte_general').get(0), 15, 15, 
		{
			'width': 170, 
			'elementHandlers': specialElementHandlers
		},
		function(dispose){
			doc.save('Reporte_General.pdf');
		});
	}

	function pdfIndividual(){
		var doc = new jsPDF();
		var specialElementHandlers = {
		'#editor': function(element, renderer){
			return true;
		}};
		doc.fromHTML($('#reporte_individual').get(0), 15, 15, 
		{
			'width': 170, 
			'elementHandlers': specialElementHandlers
		},
		function(dispose){
			doc.save('Reporte_Indivicual.pdf');
		});
	}
		function pdfLicencia(){
		var doc = new jsPDF();
		var specialElementHandlers = {
		'#editor': function(element, renderer){
			return true;
		}};
		doc.fromHTML($('#licencia').get(0), 15, 15, 
		{
			'width': 170, 
			'elementHandlers': specialElementHandlers
		},
		function(dispose){
			doc.save('Licencia.pdf');
		});
	}

	function pdfReporteDetalle(){
		var doc = new jsPDF();
		margins = {
	        top: 15,
	        bottom: 20,
	        left: 40
	    };
		var specialElementHandlers = {
		'#editor': function(element, renderer){
			return true;
		}};
		doc.fromHTML($('#reporteDetalle').get(0), 15, 15, 
		{
			'width': 170,
			'pagesplit': true,
			'elementHandlers': specialElementHandlers
		},
		function(dispose){
			doc.save('Licencia.pdf');
		}, margins);
	}