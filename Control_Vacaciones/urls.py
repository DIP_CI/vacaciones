from django.conf.urls import patterns, include, url
from django.contrib import admin
from django.conf import settings

urlpatterns = patterns('',
    url(r'^media/(?P<path>.*)$', 'django.views.static.serve', {'document_root':settings.MEDIA_ROOT,}),
    # url(r'^integrantes/fotos/(?P<path>.*)$', 'django.views.static.serve', {'document_root':settings.MEDIA_ROOT,}),

    url(r'^admin/', include(admin.site.urls)),
    url(r'^areas/', include("Apps.AreaCargo.urls")),
    url(r'^licencias/', include("Apps.Licencias.urls")),
    url(r'^trabajadores/', include("Apps.Trabajador.urls")),
	url(r'^$', 'django.contrib.auth.views.login', {'template_name': 'login.html'}),
)
